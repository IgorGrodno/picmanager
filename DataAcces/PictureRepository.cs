﻿using DataAcces.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces
{
    public class PictureRepository : IRepository<Picture>
    {
        private readonly DataContext _dataBase;

        public PictureRepository(DataContext context)
        {
            _dataBase = context;
        }

        public IEnumerable<Picture> GetAll()
        {
            return _dataBase.Pictures;
        }

        public Picture GetById(Guid id)
        {
            var picture = _dataBase.Pictures.ToList<Picture>().Find(x => x.Id == id);
            return picture;
        }

        public Picture GetByName(string name)
        {
            var picture = _dataBase.Pictures.ToList<Picture>().Find(x => x.Name == name);
            return picture;
        }

        public void Add(Picture picture)
        {
            _dataBase.Pictures.Add(picture);
        }

        public void Update(Picture picture)
        {
            _dataBase.Entry(picture).State = EntityState.Modified;
            _dataBase.SaveChanges();
        }

        public void Delete(Picture picture)
        {
            _dataBase.Pictures.Remove(picture);
        }
    }
}
