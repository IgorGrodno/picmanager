﻿using System.Data.Entity;

namespace DataAcces
{
    public class DataContext : DbContext
    {
        public DbSet<Picture> Pictures { get; set; }

        public DataContext()
            : base("DefaultConnection")
        {

        }
    }
}
