﻿using DataAcces.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _db;
        private readonly PictureRepository _pictureRepository;

        private bool disposed = false;

        public UnitOfWork()
        {
            _db = new DataContext();
            _pictureRepository = new PictureRepository(_db);
        }

        public IRepository<Picture> Pictures()
        {
            return _pictureRepository;
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

