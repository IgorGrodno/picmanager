﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces
{
    public class Picture
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public long Size { get; set; }
        public DateTime UploadDate { get; set; }
        public string Genre { get; set; }
        public int Rating { get; set; }
        public string Path {get; set;}
    }
}
