﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Picture> Pictures();
        void Save();
    }
}
