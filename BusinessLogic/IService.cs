﻿using DataAcces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public interface IService
    {
        void UpdatePicture(Picture picture);
        IEnumerable<Picture> GetAllPictures();
        IOrderedEnumerable<Picture> SortByGenre();
        IOrderedEnumerable<Picture> SortByName();
        void AddPicture(string pathToLoad, string pathToSave, string name, string ganre);
        void DeletePicture(Picture picture);
        void Dispose();
    }
}
