﻿using DataAcces;
using DataAcces.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BusinessLogic
{
    public class PictureService : IService
    {
        IUnitOfWork _dataBase;

        public PictureService(IUnitOfWork unitOfWork)
        {
            _dataBase = unitOfWork;
        }

        public void Dispose()
        {
            _dataBase.Dispose();
        }

        public void AddPicture(string pathToLoad, string pathToSave, string name, string genre)
        {
            if (!File.Exists(pathToLoad))
            {
                throw new System.Exception("File not found");
            }
            else
            {
                if (_dataBase.Pictures().GetByName(name) != null)
                {
                    throw new System.Exception("Name allready exist");
                }
                else
                {
                    try
                    {
                        File.Copy(pathToLoad, pathToSave);
                        _dataBase.Pictures().Add(new Picture
                        {
                            Id = new Guid(),
                            Name = name,
                            Genre = genre,
                            Rating = 0,
                            Size = new System.IO.FileInfo(pathToSave).Length,
                            UploadDate = DateTime.Now
                        });
                    }
                    catch
                    {
                        throw new System.Exception("Can't copy file to destination folder");
                    }
                }
            }

        }

        public void DeletePicture(Picture picture)
        {
            _dataBase.Pictures().Delete(picture);
        }



        public IEnumerable<Picture> GetAllPictures()
        {
            return _dataBase.Pictures().GetAll();
        }

        public IOrderedEnumerable<Picture> SortByGenre()
        {
            var sortedPictures = from item in _dataBase.Pictures().GetAll()
                                 orderby item.Genre
                                 select item;
            return sortedPictures;
        }

        public IOrderedEnumerable<Picture> SortByName()
        {
            var sortedPictures = from item in _dataBase.Pictures().GetAll()
                                 orderby item.Name
                                 select item;
            return sortedPictures;
        }

        public void UpdatePicture(Picture picture)
        {
            var pictureToUpdate = _dataBase.Pictures().GetById(picture.Id);

            pictureToUpdate.Name = picture.Name;
            pictureToUpdate.Genre = picture.Genre;
            pictureToUpdate.Rating = picture.Rating;
            pictureToUpdate.Path = picture.Path;

            _dataBase.Pictures().Update(pictureToUpdate);
        }
    }
}
